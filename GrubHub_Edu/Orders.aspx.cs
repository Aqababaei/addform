﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrubHub_Edu
{
    public partial class Orders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["id"] != null)
            {
                var result = General.GetFoodById(Convert.ToInt32(Request.QueryString["id"]));

                if (result!=null)
                {
                    lblFoodName.Text = result.FoodName;
                    lblFoodDescription.Text = result.FoodDescritioin;
                    lblFoodPrice.Text = result.FoodPrice;
                    lblRestaurantName.Text = result.RestaurantName;
                    lblTransportPrice.Text = result.TransportPrice;
                    imgFood.ImageUrl = result.FoodImage;
                }
            }
        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {
            try
            {
                var no = Convert.ToInt32(txtNo.Text);
                lblMessage.Text = "سفارش شما با موفقیت ثبت شد.به زودی غذا برای شما ارسال می گردد";
            }
            catch(Exception ex)
            {
                lblMessage.Text = "تعداد باید به صورت عددی باشد";
            }
        }
    }
}