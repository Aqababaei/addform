﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Food.aspx.cs" Inherits="GrubHub_Edu.Food" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>غذاها و رستورانهای سیستم</title>
    <link href="App_Themes/Main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" dir="rtl">
        <div>
            <h3>ثبت غذا و رستوران جدید </h3>
            <div class="box">
                <label class="lbl">نام رستوران</label>
                <asp:TextBox CssClass="txt" ID="txtRestaurantName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtFoodName" ErrorMessage="*" ForeColor="Red" ValidationGroup="grp"></asp:RequiredFieldValidator>
            </div>
            <div class="box">
                <label class="lbl">آدرس رستوران</label>
                <asp:TextBox CssClass="txt" ID="txtRestaurantAdd" runat="server" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtRestaurantAdd" ErrorMessage="*" ForeColor="Red" ValidationGroup="grp"></asp:RequiredFieldValidator>
            </div>
            <div class="box">
                <label class="lbl">نام غذا</label>
                <asp:TextBox CssClass="txt" ID="txtFoodName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtFoodName" ErrorMessage="*" ForeColor="Red" ValidationGroup="grp"></asp:RequiredFieldValidator>
            </div>
            <div class="box">
                <label class="lbl">قیمت غذا( به تومان)</label>
                <asp:TextBox CssClass="txt" ID="txtPrice" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPrice" ErrorMessage="*" ForeColor="Red" ValidationGroup="grp"></asp:RequiredFieldValidator>
            </div>
             <div class="box">
                <label class="lbl">قیمت حمل و نقل( به تومان)</label>
                <asp:TextBox CssClass="txt" ID="txtTransportPrice" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtTransportPrice" ErrorMessage="*" ForeColor="Red" ValidationGroup="grp"></asp:RequiredFieldValidator>
            </div>
            <div class="box">
                <label class="lbl">مشخصات غذا</label>
                <br />
                <asp:TextBox CssClass="txt" ID="txtFoodDescription" runat="server" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtFoodDescription" ErrorMessage="*" ValidationGroup="grp" ForeColor="Red"></asp:RequiredFieldValidator>
            </div>
            <div class="box">
                <label class="lbl">مشخصات ظاهری غذا</label>
                <br />
                <asp:FileUpload ID="foodImageUploader" runat="server" />
                <br />
                <asp:Image runat="server" ID="foodImage" />
            </div>
            <div class="box">
                <asp:Label runat="server" id="lblMessage" ForeColor="red"></asp:Label>
                <asp:Button runat="server" ID="btnSave" Text="ذخیره" CssClass="btn" OnClick="btnSave_Click" ValidationGroup="grp" />
                <asp:Button runat="server" ID="btnCancel" Text="انصراف" CssClass="btn" OnClick="btnCancel_Click" />
            </div>
        </div>
        <div>
            <h3>لیست رستورانها و غذاهای ثبت شده سیستم</h3>
            <asp:GridView ID="grdFoods" runat="server" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
                <AlternatingRowStyle BackColor="White" />
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
                <Columns>
                    <asp:BoundField DataField="RestaurantName" HeaderText="نام رستوران" />
                    <asp:BoundField DataField="RestaurantAddress" HeaderText="آدرس رستوران" />
                    <asp:BoundField DataField="FoodName" HeaderText="نام غذا" />
                    <asp:BoundField DataField="FoodDescritioin" HeaderText="توضیحات مربوط به غذا" />
                    <asp:BoundField DataField="FoodPrice" HeaderText="قیمت غذا" />
                    <asp:BoundField DataField="TransportPrice" HeaderText="هزینه حمل و نقل" />
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
