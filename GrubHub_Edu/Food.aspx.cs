﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrubHub_Edu
{
    public partial class Food : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var foodList = General.GetFoodList();
            if (foodList != null)
            {
                grdFoods.DataSource = foodList;
                grdFoods.DataBind();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var food=new FoodProperty();

            food.FoodDescritioin = txtFoodDescription.Text;
            food.FoodName = txtFoodName.Text;
            food.FoodPrice = txtPrice.Text;
            food.TransportPrice = txtTransportPrice.Text;
            food.RestaurantName = txtRestaurantName.Text;
            food.RestaurantAddress = txtRestaurantAdd.Text;
            food.FoodImage = "FoodImage\\"+foodImageUploader.FileName;

            if(foodImageUploader.HasFile)
                foodImageUploader.PostedFile.SaveAs(Server.MapPath("~/FoodImage/") + foodImageUploader.FileName);

            if (General.SetFoodProperty(food))
                lblMessage.Text = "اطلاعات با موفقیت ذخیره شد";
            var foodList = General.GetFoodList();
            if (foodList != null)
            {
                grdFoods.DataSource = foodList;
                grdFoods.DataBind();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("home.aspx");
        }
    }
}