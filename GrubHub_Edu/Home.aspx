﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="GrubHub_Edu.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>صفحه اصلی</title>
    <link href="App_Themes/Main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" dir="rtl">
        <div>

            <div class="box">
                <asp:Button runat="server" ID="btnEnter" Text="ورود" CssClass="btn" OnClick="btnEnter_Click" />
                <asp:Button runat="server" ID="btnRegister" Text="ثبت نام" CssClass="btn" OnClick="btnRegister_Click" />
                <asp:Button runat="server" ID="btnOrderList" Text="سبد خرید" CssClass="btn" OnClick="btnOrderList_Click" />
            </div>
            <div style="padding: 100px;">
                <div>
                    <h3>غذای مورد نظر خود را در نزدیکترین رستوران سفارش دهید!!!</h3>
                    <div class="box">
                        <label class="lbl">آدرس شما کجاست؟</label>
                        <asp:TextBox CssClass="txt" ID="txtAddress" runat="server" Width="500px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtAddress" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force"></asp:RequiredFieldValidator>
                    </div>
                    <div class="box">
                        <label class="lbl">غذای مورد نظرتان چیست؟</label>
                        <asp:TextBox CssClass="txt" ID="txtFood" runat="server" Width="500px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtFood" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force"></asp:RequiredFieldValidator>
                    </div>
                    <div class="box" style="text-align: center;">
                        <asp:Label runat="server" id="lblMessage"  ForeColor="red"></asp:Label>
                        <br />
                        <asp:Button runat="server" ID="btnSearch" Text="جست و جو" CssClass="btn" OnClick="btnSearch_Click" ValidationGroup="Force" />
                    </div>
                    <div>
                        <asp:Repeater ID="rptFoods" runat="server" OnItemCommand="rptFoods_ItemCommand" >
                            <ItemTemplate>
                                <div style="border:1px solid black;height:180px;">
                                    <div style="float: left;margin:5px; ">
                                        <asp:Image runat="server" Height="150px" Width="200px" ImageUrl=<%#Eval("FoodImage")%> />
                                    </div>
                                    <div >
                                        <label>نام غذا</label>
                                        <asp:Label runat="server"><%#Eval("FoodName")%></asp:Label>
                                        <br />
                                       <label>مشخصات غذا</label>
                                        <asp:Label runat="server"><%#Eval("FoodDescritioin")%></asp:Label>
                                        <br />
                                         <label>قیمت غذا</label>
                                        <asp:Label runat="server"><%#Eval("FoodPrice")%></asp:Label>
                                        <br />
                                         <label>هزینه حمل و نقل</label>
                                        <asp:Label runat="server"><%#Eval("TransportPrice")%></asp:Label>
                                        <br />
                                         <label>نام رستوران</label>
                                        <asp:Label runat="server"><%#Eval("RestaurantName")%></asp:Label>
                                        <br />
                                        <asp:Button  Text="اضافه به سبد خرید" runat="server" CommandName="Buy" CommandArgument='<%#Eval("Id")%>' />
                                    </div>
                                </div>
                                <br />
                               <br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
