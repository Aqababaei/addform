﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="GrubHub_Edu.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ثبت نام</title>
    <link href="App_Themes/Main.css" rel="stylesheet" />
  
</head>
<body>
    <form id="form1" runat="server" dir="rtl">
        <div>
            <h3>ثبت نام</h3>
            <div class="box">
                <label class="lbl">نام</label>
                <asp:TextBox CssClass="txt" ID="txtName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force" ></asp:RequiredFieldValidator>               
            </div>
            <div class="box">
                <label class="lbl">نام خانوادگی</label>
                <asp:TextBox CssClass="txt" ID="txtFamily" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtFamily" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force"  ></asp:RequiredFieldValidator>
            </div>
            <div class="box">
                <label class="lbl">شماره تلفن همراه</label>
                <asp:TextBox CssClass="txt" ID="txtMobileNo" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMobileNo" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force" ></asp:RequiredFieldValidator>
            </div>
             <div class="box">
                <label class="lbl">شماره تلفن ثابت</label>
                <asp:TextBox CssClass="txt" ID="txtPhone" runat="server"></asp:TextBox>
                  <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPhone" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force"  ></asp:RequiredFieldValidator>
            </div>
             <div class="box">
                <label class="lbl">آدرس ایمیل</label>
                <asp:TextBox CssClass="txt" ID="txtEmail" runat="server" TextMode="Email"></asp:TextBox>
                 <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail" ErrorMessage="*"  ForeColor="Red" ValidationGroup="Force" ></asp:RequiredFieldValidator>
            </div>
             <div class="box">
                <label class="lbl">رمز عبور</label>
                <asp:TextBox CssClass="txt" ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                 <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPassword" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force" ></asp:RequiredFieldValidator>
            </div>
             <div class="box">
                <label class="lbl">تکرار رمز عبور</label>
                <asp:TextBox CssClass="txt" ID="txtPassword2" runat="server" TextMode="Password"></asp:TextBox>
                  <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPassword2" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force"></asp:RequiredFieldValidator>
            </div>
             <div class="box">
                <label class="lbl">آدرس</label>
                <br />
                <asp:TextBox CssClass="txt" ID="txtAddress" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtAddress" ErrorMessage="*" ValidationGroup="Force" ForeColor="Red" ></asp:RequiredFieldValidator>
            </div>
             <div class="box">
                 <asp:Label runat="server" id="lblMessage" ForeColor="red"></asp:Label>
                 <br />
                <asp:Button runat="server" ID="btnSave" Text="ذخیره" CssClass="btn" OnClick="btnSave_Click" ValidationGroup="Force" />
                <asp:Button runat="server" ID="btnCancel" Text="انصراف" CssClass="btn" OnClick="btnCancel_Click" />
            </div>
        </div>
    </form>
</body>
</html>
