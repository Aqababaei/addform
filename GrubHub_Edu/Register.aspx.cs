﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ServiceStack.Redis;


namespace GrubHub_Edu
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["email"] != null)
            {
                var user = General.GetUser(Session["email"].ToString());
                if (user != null)
                {
                    txtAddress.Text = user.Address;
                    txtEmail.Text = user.Email;
                    txtFamily.Text = user.Family;
                    txtName.Text = user.Name;
                    txtMobileNo.Text = user.MobileNo;
                    txtPhone.Text = user.PhoneNo;
                }
            }
            if (!IsPostBack)
            {
                lblMessage.Text = "";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("home.aspx");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var newUser = new User();
            newUser.Email = txtEmail.Text;
            newUser.Address = txtAddress.Text;
            newUser.Family = txtFamily.Text;
            newUser.MobileNo = txtMobileNo.Text;
            newUser.Name = txtName.Text;
            newUser.Password = txtPassword.Text;
            newUser.PhoneNo = txtPhone.Text;
            var isRegistered = false;

            if (Session["email"] != null)
                isRegistered = true;

            if (General.SetUser(newUser, isRegistered))
            {
                if (Session["email"] != null)
                    Session.Add("email", newUser.Email);
                else
                    Session.Add("email", newUser.Email);

                Response.Redirect("Home.aspx");
            }
            else
            {
                lblMessage.Text = "این ایمیل قبلا در سامانه ثبت شده است";
            }
        }
    }
}