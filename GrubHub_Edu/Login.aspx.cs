﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrubHub_Edu
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnter_Click(object sender, EventArgs e)
        {
            var user = General.GetUser(txtUserName.Text,txtPassword.Text);
            if (user != null)
            {
                Session.Add("email", user.Email);
                Response.Redirect("home.aspx");
            }
            else
                lblMessage.Text = "نام کاربری و یا رمز عبور اشتباه می باشد";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("home.aspx");
        }
    }
}