﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="GrubHub_Edu.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ورود به سامانه</title>
    <link href="App_Themes/Main.css" rel="stylesheet" />
</head>
<body>
     <form id="form1" runat="server" dir="rtl">
        <div>
            <h3>ثبت نام</h3>
             <div class="box">
                <label class="lbl">نام کاربری(ایمیل)</label>
                <asp:TextBox CssClass="txt" ID="txtUserName" runat="server" TextMode="Email"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtUserName" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force"></asp:RequiredFieldValidator>               
            </div>
            <div class="box">
                <label class="lbl">رمز عبور</label>
                <asp:TextBox CssClass="txt" ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPassword" ErrorMessage="*" ForeColor="Red" ValidationGroup="Force" ></asp:RequiredFieldValidator>
            </div>
             <div class="box">
                 <asp:Label ForeColor="Red" runat="server" ID="lblMessage"></asp:Label>
                <asp:Button runat="server" ID="btnEnter" Text="ورود" CssClass="btn" OnClick="btnEnter_Click" ValidationGroup="Force" />
                <asp:Button runat="server" ID="btnCancel" Text="انصراف" CssClass="btn" OnClick="btnCancel_Click" />
            </div>
        </div>
    </form>
</body>
</html>
