﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="GrubHub_Edu.Orders" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>سفارشات کاربر</title>
    <link href="App_Themes/Main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" dir="rtl">
        <div>
            <div style="border: 1px solid black; height: 180px;">
                <div style="float: left; margin: 5px;">
                    <asp:Image runat="server" Height="150px" Width="200px" id="imgFood" />
                </div>
                <div>
                    <label>نام غذا</label>
                    <asp:Label runat="server" ID="lblFoodName"></asp:Label>
                    <br />
                    <label>مشخصات غذا</label>
                    <asp:Label runat="server" ID="lblFoodDescription"></asp:Label>
                    <br />
                    <label>قیمت غذا</label>
                    <asp:Label runat="server" ID="lblFoodPrice"></asp:Label>
                    <br />
                    <label>هزینه حمل و نقل</label>
                    <asp:Label runat="server" ID="lblTransportPrice"></asp:Label>
                    <br />
                    <label>نام رستوران</label>
                    <asp:Label runat="server" ID="lblRestaurantName"></asp:Label>
                    <br />
                         <label>تعداد مورد نیاز </label>
                    <asp:TextBox  runat="server" ID="txtNo"></asp:TextBox>
                     <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNo" ErrorMessage="*" ValidationGroup="grp" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:Button Text="ثبت سفارش و پرداخت" runat="server" ID="btnPayment" ValidationGroup="grp" OnClick="btnPayment_Click" />
                    <asp:Label runat="server" ID="lblMessage" ForeColor="Red"></asp:Label>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
