﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrubHub_Edu
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userEmail = "";

            if (Session["email"] != null)
            {
                userEmail = Session["email"].ToString();
                var user = General.GetUser(userEmail);

                if (user != null)
                {
                    btnEnter.Text = user.Name + " " + user.Family;
                    btnRegister.Visible = false;
                    btnOrderList.Visible = true;
                }
            }
            else
                btnOrderList.Visible = false;

            if (!IsPostBack)
            {
                var location = General.GetUserCityByIp();
                if (!string.IsNullOrEmpty(location))
                {
                    txtAddress.Text = location;
                }
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Response.Redirect("Register.aspx");
        }

        protected void btnEnter_Click(object sender, EventArgs e)
        {
            if (Session["email"] == null)
                Response.Redirect("Login.aspx");
            else
                Response.Redirect("Register.aspx");
        }

        protected void btnOrderList_Click(object sender, EventArgs e)
        {
            Response.Redirect("Orders.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var searchResult = General.SearchFood(txtAddress.Text, txtFood.Text);
            if (searchResult != null)
            {
                rptFoods.DataSource = searchResult;
                rptFoods.DataBind();
            }
            else
                lblMessage.Text = "اطلاعاتی یافت نشد!!";
        }

        protected void rptFoods_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Buy")
            {
                if (Session["email"] != null)
                    Response.Redirect("Orders.aspx?id=" + e.CommandArgument);
                else
                    Response.Redirect("Login.aspx");
            }
        }
    }
}