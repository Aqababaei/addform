﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack;
using ServiceStack.Redis;
using ServiceStack.Redis.Generic;
using StackExchange.Redis;
using Newtonsoft.Json;
using System.Net;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;

namespace GrubHub_Edu
{

    public class General
    {
        static RedisClient redisClient = new RedisClient("localhost");
       static IRedisClientsManager RedisManager { get; set; }

        #region user
        public static User GetUser(string email)
        {
            //Get a typed version of redis client that works with <User>
            var redisUsers = redisClient.As<User>();

            //Find user by DisplayName if exists
            var userKey = redisClient.GetValue(email);

            if (userKey != null)
                return redisUsers.GetValue(email);
            else
                return null;
        }

        public static User GetUser(string email, string password)
        {
            var redisUsers = redisClient.As<User>();

            var userKey = redisClient.GetValue(email);

            if (userKey != null)
            {
                var user = redisUsers.GetValue(email);
                if (user != null)
                {
                    if (user.Password.ToLower().Trim() == password.ToLower().Trim())
                        return user;
                    else
                        return null;
                }
                else
                    return null;
            }
            else
                return null;
        }

        public static bool SetUser(User newUser, bool isRegistered)
        {
            var result = false;

            var redisUsers = redisClient.As<User>();
            var user = GetUser(newUser.Email);
            if (user != null)
            {
                if (newUser.Email.ToLower().Trim() == user.Email.ToLower().Trim() && isRegistered)
                {
                    // redisUsers.SetValue(newUser.Email, newUser);
                    redisUsers.Store(newUser);
                    redisUsers.AddToRecentsList(newUser);
                    result = true;
                }
                else if (newUser.Email.ToLower().Trim() != user.Email.ToLower().Trim() && isRegistered)
                    result = false;
            }
            else
            {
                // redisUsers.SetValue(newUser.Email, newUser);
                redisUsers.Store(newUser);
                redisUsers.AddToRecentsList(newUser);
                result = true;
            }
            return result;
        }

        #endregion
        #region Food
        public static List<FoodProperty> GetFoodList()
        {      
           
            var redisFoods = redisClient.As<FoodProperty>();
            
            if (redisFoods.GetAll().Count != 0)
                return redisFoods.GetAll().ToList();
            else
                return null;

        }

        public static bool SetFoodProperty(FoodProperty food)
        {
            var result = false;
         
            var redisFoods = redisClient.As<FoodProperty>();         
            food.Id= redisFoods.GetNextSequence();
            redisFoods.Store(food);
           
                redisFoods.AddToRecentsList(food);              
              
            result = true;

            return result;
        }

        public static List<FoodProperty> SearchFood(string address,string foodName)
        {
            var redisFoods = redisClient.As<FoodProperty>();
          
            List<FoodProperty> result = new List<FoodProperty>();
        
            if (redisFoods.GetAll().Count != 0)
            {
                foreach(var food in redisFoods.GetAll().ToList())
                {
                    if (food.FoodName.Contains(foodName) && food.RestaurantAddress.Contains(address))
                    {
                        result.Add(food);
                    }                        
                }               
            }
            else
                return null;
            return result;
        }

        public static FoodProperty GetFoodById(int id)
        {
            var result = new FoodProperty();

            var redisFoods = redisClient.As<FoodProperty>();
             result =   redisFoods.GetById(id);
            return result;
        }
        #endregion
        #region Location
        public static string GetUserCityByIp()
        {
            string ip = "";

            ip = GetIPAddress();

            IpInfo ipInfo = new IpInfo();
            try
            {
                string info = new WebClient().DownloadString("http://ipinfo.io/" + ip);
                ipInfo = JsonConvert.DeserializeObject<IpInfo>(info);
                RegionInfo myRI1 = new RegionInfo(ipInfo.Country);
                ipInfo.Country = myRI1.EnglishName;
            }
            catch (Exception)
            {
                ipInfo.City = null;
            }

            return ipInfo.City;
        }
  
        static string GetIPAddress()
        {
            string internetIP = null;
            try
            {
                internetIP = new WebClient().DownloadString("http://icanhazip.com");
            }
            catch
            {               
            }
            return internetIP;
        }
        #endregion
    }

    public class User
    {
        public string Name { get; set; }
        public string Family { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNo { get; set; }
    }



    public class FoodProperty
    {
        public long Id { get; set; }
        public string FoodName { get; set; }
        public string FoodDescritioin { get; set; }
        public string FoodPrice { get; set; }
        public string FoodImage { get; set; }
        public string TransportPrice { get; set; }
        public string RestaurantName { get; set; }
        public string RestaurantAddress { get; set; }

    }

    public class IpInfo
    {

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("hostname")]
        public string Hostname { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("loc")]
        public string Loc { get; set; }

        [JsonProperty("org")]
        public string Org { get; set; }

        [JsonProperty("postal")]
        public string Postal { get; set; }
    }

}